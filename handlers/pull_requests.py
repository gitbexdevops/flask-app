import os
import requests

TOKEN = os.getenv("TOKEN")
HEADERS = {'Authorization': f'Bearer {TOKEN}'}


def get_pull_requests(state):
    dic_list = []
    dictionary = {}
    response = requests.get('https://api.github.com/repos/boto/boto3/pulls')
    data = response.json()
    for item in data:
        dictionary['title'] = item["title"]
        dictionary['num'] = item["number"]
        dictionary['link'] = item["url"]
        dic_list.append(dictionary.copy())
    return dic_list
